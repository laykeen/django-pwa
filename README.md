# Django PWA test

Testing django + tachyons for web development

## Dependencies

1. python 3+
1. pip
1. Python Virtual Enviroment
```bash
sudo apt install python3-venv
python3 -m venv ~/.virtualenvs/django
source ~/.virtualenvs/django/bin/activate
```
1. django
```bash
python3 -m pip install Django
```
## Setup

https://docs.djangoproject.com/en/3.1/ref/django-admin/#django-admin-startproject

```bash
django-admin startproject myproject
cd myproject
```

## Deploy

https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/uvicorn/

https://www.uvicorn.org/

```bash
python3 -m pip install uvicorn gunicorn
gunicorn myproject.asgi:application -k uvicorn.workers.UvicornWorker
firefox 127.0.0.1:8000
```